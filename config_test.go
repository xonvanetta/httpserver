package ginhttpserver

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestValidate(t *testing.T) {
	c := Config{}
	err := c.validate()
	assert.Equal(t, ErrMissingPort, err)
	c.Port = "8080"
	err = c.validate()
	assert.Equal(t, ErrMissingShutdownTimeout, err)
	c.ShutdownTimeout = time.Minute
	err = c.validate()
	assert.NoError(t, err)
}
