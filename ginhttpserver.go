package ginhttpserver

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin"
)

// Server contains everything the server needs
type Server struct {
	config     Config
	HTTPServer *http.Server
	Gin        *gin.Engine
	Logger     Logger
}

// New creates a new instance of server with gin applied to it
func New(gin *gin.Engine, config Config) (*Server, error) {
	err := config.validate()
	if err != nil {
		return nil, err
	}
	hs := &http.Server{
		Handler: gin,
		Addr:    fmt.Sprintf(":%s", config.Port),
	}

	server := &Server{
		Gin:        gin,
		config:     config,
		HTTPServer: hs,
	}

	return server, nil
}

//NewWithLogger
func NewWithLogger(config Config) (*Server, error) {
	g := gin.New()
	g.Use(gin.Recovery())
	g.Use(NewLogger(config).Middleware())

	return New(g, config)
}

// New creates a new gin with recovery and logging with an instance of server
func NewGin(config Config) (*Server, error) {
	g := gin.New()
	g.Use(gin.Recovery())
	g.Use(gin.Logger())

	return New(g, config)
}

// Start starts the ginhttpserver in its own goroutine
// and waits for the shutdown to be completed.
// Its block until the ginhttpserver has completed all the request.
// Starts the graceful that blocks until any SIGTERM comes by.
func (s *Server) Start() error {
	log.Printf("ginhttpserver: starting on port :%s\n", s.config.Port)
	errCh := make(chan error)
	go func() {
		err := s.HTTPServer.ListenAndServe()
		if err == nil {
			return
		}
		errCh <- err
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, syscall.SIGQUIT, syscall.SIGTERM)
	select {
	case err := <-errCh:
		return err
	case <-quit:
	}

	log.Println("ginhttpserver: shutdown")
	ctx, cancel := context.WithTimeout(context.Background(), s.config.ShutdownTimeout)
	defer cancel()

	err := s.HTTPServer.Shutdown(ctx)
	if err != nil {
		return err
	}

	return nil
}
