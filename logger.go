package ginhttpserver

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type Logger struct {
	*logrus.Logger
}

const key string = "logger"

func NewLogger(config Config) *Logger {
	l := logrus.StandardLogger()
	l.SetLevel(config.LogLevel)

	l.Formatter = &logrus.JSONFormatter{}
	return &Logger{l}
}

func (l *Logger) Middleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()

		logger := l.WithFields(logrus.Fields{
			"method":  c.Request.Method,
			"path":    c.Request.URL.Path,
			"ip":      c.ClientIP(),
			"headers": c.Request.Header,
		})
		c.Set(key, logger)
		c.Next()

		latency := time.Now().Sub(start)
		statusCode := c.Writer.Status()

		entry := logger.WithFields(logrus.Fields{
			"status":  statusCode,
			"latency": latency.String(),
		})

		if statusCode > 499 {
			entry.Error(c.Errors.String())
			return
		}
		if statusCode > 399 {
			entry.Warn(c.Errors.String())
			return
		}
		entry.Info(c.Errors.String())
	}
}

func GetLogger(c *gin.Context) *logrus.Entry {
	l, ok := c.Get(key)
	if !ok {
		return nil
	}

	logger, ok := l.(*logrus.Entry)
	if !ok {
		return nil
	}
	return logger
}
